# lyx

WYSIWYM -frontend for LaTeX https://tracker.debian.org/pkg/lyx

## Dependencies
* librsvg2-bin is needed to convert included SVG images to PDF or EPS.
  A SVG file is included from splash.lyx which is used to test lyx.

## Official documentation
* https://wiki.debian.org/Latex